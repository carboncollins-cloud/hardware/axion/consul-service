# Axion NAS - Consul Service

[[_TOC_]]

## Description

Registers several external services into Consul each with their own health checks. This also tags the
https and syncthing endpoints so that Traefik in [Internal Proxy](https://gitlab.com/carboncollins-cloud/network/network-internal-proxy) is able to auto configure a https proxy.
