terraform {
  backend "consul" {
    path = "terraform/axion-consul-service"
  }

  required_providers {
    consul = {
      source = "hashicorp/consul"
      version = "2.15.1"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.5.0"
    }
  }
}

provider "consul" {}
provider "vault" {}

data "vault_generic_secret" "axion_syncthing" {
  path = "api/syncthing/axion"
}

resource "consul_node" "axion_nas" {
  name    = "axion"
  address = data.vault_generic_secret.axion_syncthing.data["address"]

  meta = {
    "external-node"  = "true"
    "external-probe" = "true"
  }
}

resource "consul_service" "axion_https" {
  name = "axion"
  service_id = "axion-https"
  node = consul_node.axion_nas.name
  port = 5001
  tags = [
    "https",
    "traefik.enable=true",
    "traefik.http.routers.axion.entrypoints=https",
    "traefik.http.routers.axion.rule=Host(`axion.hq.carboncollins.se`)",
    "traefik.http.routers.axion.tls=true",
    "traefik.http.routers.axion.tls.certresolver=lets-encrypt",
    "traefik.http.routers.axion.tls.domains[0].main=*.hq.carboncollins.se",
    "traefik.http.services.axion.loadBalancer.serversTransport=insecureSkipVerify@file",
    "traefik.http.services.axion.loadbalancer.server.scheme=https"
  ]

  check {
    check_id = "httpsEndpoint"
    name = "Https health check"
    status = "warning"
    http = "https://${consul_node.axion_nas.address}:5001/"
    tls_skip_verify = true
    method = "HEAD"
    interval = "30s"
    timeout = "10s"
    deregister_critical_service_after = "48h"

    header {
      name  = "host"
      value = ["${consul_node.axion_nas.address}:5001"]
    }

    header {
      name  = "user-agent"
      value = ["consul-health-check"]
    }
  }
}

resource "consul_service" "axion_snmp" {
  name = "axion"
  service_id = "axion-snmp"
  node = consul_node.axion_nas.name
  port = 161
  tags = ["snmp"]

  check {
    check_id = "snmpEndpoint"
    name = "SNMP health check"
    status = "warning"
    tcp = "${consul_node.axion_nas.address}:161"
    interval = "15s"
    timeout = "5s"
    deregister_critical_service_after = "48h"
  }
}

resource "consul_service" "axion_smb" {
  name = "axion"
  service_id = "axion-smb"
  node = consul_node.axion_nas.name
  port = 445
  tags = ["smb"]

  check {
    check_id = "smbEndpoint"
    name = "SMB health check"
    status = "warning"
    tcp = "${consul_node.axion_nas.address}:445"
    interval = "15s"
    timeout = "5s"
    deregister_critical_service_after = "48h"
  }
}

resource "consul_service" "axion_syncthing" {
  name = "axion"
  service_id = "axion-syncthing"
  node = consul_node.axion_nas.name
  port = 8384
  tags = [
    "syncthing",
    "traefik.enable=true",
    "traefik.http.routers.axion-syncthing.entrypoints=https",
    "traefik.http.routers.axion-syncthing.rule=Host(`syncthing.axion.hq.carboncollins.se`)",
    "traefik.http.routers.axion-syncthing.tls=true",
    "traefik.http.routers.axion-syncthing.tls.certresolver=lets-encrypt",
    "traefik.http.routers.axion-syncthing.tls.domains[0].main=*.hq.carboncollins.se",
    "traefik.http.services.axion-syncthing.loadBalancer.serversTransport=insecureSkipVerify@file",
    "traefik.http.services.axion-syncthing.loadbalancer.server.scheme=https"
  ]

  check {
    check_id = "syncthingEndpoint"
    name = "Syncthing health check"
    status = "warning"
    http = "https://${consul_node.axion_nas.address}:8384/"
    tls_skip_verify = true
    method = "HEAD"
    interval = "30s"
    timeout = "10s"
    deregister_critical_service_after = "48h"

    header {
      name = "x-api-key"
      value = [data.vault_generic_secret.axion_syncthing.data["apiKey"]]
    }
  }
}
